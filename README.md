# Contrôle de présence

## Qu'est-ce ?

*App_name* facilite le Contrôle de
Présence et du temps de travail, en s’adaptant aux besoins de chaque fonction de votre société. Il apporte une visibilité
de la productivité de chaque employé et équipe, ce qui permet aux responsables de prendre les décisions qui maximisent
le rendement des établissements.

Il permet de :
- Réaliser un suivi des heures travaillées et du repos de vos employés.
- Valider les heures travaillées et le pointage de vos employés avant le calcul des salaires.
- Accéder à la vue préalable des heures brutes et des compteurs d’heures ordinaires et complémentaires.
- Comparer facilement le travail planifié par rapport à celui réellement exécuté.
- Pointer depuis l’app mobile de l’employé.

## Setup

- Cloner le repo `git clone git@gitlab.com:damedomey/controle-de-presence.git`
- Modifier les informations de configuration dans `.env` pour l'adapter à votre environnement
- Exécuter les migrations
- Démarrer le serveur de développement `php spark serve`
- Ouvrir `http://localhost:8080`