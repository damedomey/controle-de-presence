<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Entities\TypeConge;
use App\Models\TypeCongeModel;

class TypeCongeController extends BaseController
{
    protected $model;
    public function __construct()
    {
        $this->model = new TypeCongeModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Type de congés',
            'typeConges' => $this->model->findAll()
        ];

        return view('typeConge/index', $data);
    }

    public function create()
    {
        if ($this->request->getPost() && $this->validate([
            'title' => "required|min_length[3]|max_length[255]"
            ])){
            $typeConge = new TypeConge($this->request->getPost());
            $this->model->save($typeConge);
        }
        return redirect()->to('/type-conge');
    }

    public function edit($id = null)
    {
        if ($this->request->getPost() && $this->validate([
                'title' => "required|min_length[3]|max_length[255]"
            ])){
            $typeConge = new TypeConge($this->request->getPost());
            $this->model->update($typeConge->id, $typeConge);
            return redirect()->to('/type-conge');
        }

        $data = [
            'title' => 'Type de congés',
            'congeValue' => $this->model->find($id),
            'typeConges' => $this->model->findAll()
        ];
        return view('typeConge/index', $data);
    }

    public function delete($id)
    {
        $this->model->delete($id);
        return redirect()->to('/type-conge');
    }
}
