<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\EmployeModel;

class Employe extends BaseController
{
    protected $model;
    public function __construct()
    {
        $this->model = new EmployeModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Liste des employés',
            'employes' => $this->model->findAll()
        ];
        return view('employe/index', $data);
    }

    public function create()
    {
        if ($this->request->getPost() && $this->validate($this->model->getValidationRules())) {
            $employe = new \App\Entities\Employe($this->request->getPost());
            $this->model->save($employe);
            return redirect()->to("/employe");
        }
        return view('employe/create');
    }

    public function show($id)
    {
        $data = [
            'employe' => $this->model->find($id)
        ];
        return view('employe/show', $data);
    }

    public function edit($id)
    {
        // TODO: Modification des informations d'un employé
    }
    public function delete($id)
    {
        $this->model->delete($id);
        return redirect()->to("/employe");
    }
}
