<?php include_once APPPATH . 'Views/_partials/header.php' ?>
<?php include_once APPPATH . 'Views/_partials/navbar.php' ?>
    <div id="page-wrapper">
        <div class="header">
            <h1 class="page-header">
                Types de congés
            </h1>

        </div>
        <div id="page-inner">

            <div class="row">

                <div class="col-md-12">
                    <div class="row">
                        <!-- Liste des congés -->
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-action">
                                    Liste
                                </div>
                                <div class="card-content">
                                    <div class="card-content">
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Titre</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $i = 1;
                                                foreach ($typeConges as $conge): ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $conge->title ?></td>
                                                    <td>
                                                        <a href="/type-conge/edit/<?= $conge->id ?>"><i class="fa fa-edit text-warning"></i></a>
                                                        <a href="/type-conge/delete/<?= $conge->id ?>"><i class="fa fa-trash-o text-danger"></i></a>
                                                        <!-- TODO: Mettre un formulaire à la place et configurer la route de suppression pour ne répondre qu'aux requêtes 'post' -->
                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>

                                                </tbody>
                                            </table>
                                            <?php
                                            if (empty($typeConges)) {
                                                echo "Pas de données dans la table";
                                            }
                                            ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- Formulaire d'ajout et de modification -->
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-action">
                                    Formulaire
                                </div>
                                <div class="card-content">
                                    <?= session()->getFlashdata('error') ?>
                                    <?= service('validation')->listErrors() ?>
                                    <?php if (! isset($congeValue)): ?>
                                        <form action="/type-conge/create" method="POST">
                                            <?php csrf_field(); ?>
                                            <div class="input-field">
                                                <input placeholder="" name="title" id="title" type="text" class="validate">
                                                <label for="title">Titre</label>
                                            </div>
                                            <div>
                                                <button type="submit" class="btn btn-primary">Ajouter</button>
                                                <button type="reset" class="btn btn-danger">Annuler</button>
                                            </div>
                                        </form>
                                    <?php else: ?>
                                        <form action="/type-conge/edit" method="POST">
                                            <?php csrf_field(); ?>
                                            <input type="hidden" name="id" value="<?= $congeValue->id ?>">
                                            <div class="input-field">
                                                <input placeholder="" type="text" value="<?= $congeValue->title ?>" readonly>
                                            </div>
                                            <div class="input-field">
                                                <input placeholder="" name="title" id="title" type="text" class="validate"">
                                                <label for="title">Nouveau titre</label>
                                            </div>
                                            <div>
                                                <button type="submit" class="btn btn-primary">Modifier</button>
                                                <button type="reset" class="btn btn-danger">Annuler</button>
                                            </div>
                                        </form>
                                    <?php endif;?>
                                    <div class="clearBoth"></div>
                                </div>
                            </div>
                        </div>
                    </div>

<?php include_once APPPATH . 'Views/_partials/footer.php'; ?>