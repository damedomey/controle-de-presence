<?php include_once APPPATH . 'Views/_partials/header.php' ?>
<?php include_once APPPATH . 'Views/_partials/navbar.php' ?>
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            Informations
        </h1>
    </div>
    <div id="page-inner">

        <div class="row">

            <div class="col-md-12">
                <!--Page content-->
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7">
                        <div class="cirStats">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="card-panel text-center">
                                        <h4>Jours travaillés</h4>
                                        <div class="easypiechart text-primary"><span class="percent">965</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="card-panel text-center">
                                        <h4>Absence</h4>
                                        <div class="easypiechart" id="easypiechart-red" data-percent="46" ><span class="percent">5</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="card-panel text-center">
                                        <h4>Taux d'assiduité</h4>
                                        <div class="easypiechart" id="easypiechart-teal" data-percent="84" ><span class="percent">97%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="card-panel text-center">
                                        <h4>Jours de congé</h4>
                                        <div class="easypiechart" id="easypiechart-orange" data-percent="55" ><span class="percent">55%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--/.row-->
                    <div class="col-xs-12 col-sm-12 col-md-5">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="card">
                                    <div class="card-action">
                                        Identification
                                    </div>
                                    <div class="card-body">
                                        <p style="margin-left: 50px"> <strong>Nom : </strong> <?= $employe->nom ?? "-" ?></p>
                                        <p style="margin-left: 50px"> <strong>Prénom(s) : </strong> <?= $employe->prenoms ?? "-" ?></p>
                                        <p style="margin-left: 50px"> <strong>Email : </strong> <?= $employe->email ?? "-" ?></p>
                                        <p style="margin-left: 50px"> <strong>Contact : </strong> <?= $employe->contact ?? "-" ?></p>
                                        <p style="margin-left: 50px"> <strong>Ajouté le : </strong> <?= $employe->created_at ?? "-" ?></p>
                                        <p style="margin-left: 50px"> <strong>-</strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--/.row-->
                </div>

                    <script defer>
                    $(document).ready(function () {
                        $('#dataTables-example').dataTable();
                    });
                </script>
                <?php include_once APPPATH . 'Views/_partials/footer.php'; ?>

