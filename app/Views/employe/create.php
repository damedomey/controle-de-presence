<?php include_once APPPATH . 'Views/_partials/header.php' ?>
<?php include_once APPPATH . 'Views/_partials/navbar.php' ?>
    <div id="page-wrapper">
        <div class="header">
            <h1 class="page-header">
                Ajouter un employé
            </h1>
        </div>
        <div id="page-inner">

            <div class="row">

                <div class="col-md-12">
                    <!--Page content-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-action">

                                </div>
                                <div class="card-content">
                                    <form class="col s12" method="post" action="/employe/create">
                                        <?= session()->getFlashdata('error') ?>
                                        <?= service('validation')->listErrors() ?>
                                        <?php csrf_field() ?>
                                        <div class="row">
                                            <div class="input-field col s6">
                                                <input placeholder="" id="nom" name="nom" type="text" class="validate">
                                                <label for="nom">Nom</label>
                                            </div>
                                            <div class="input-field col s6">
                                                <input id="prenoms" name="prenoms" type="text" class="validate">
                                                <label for="prenoms">Prénom(s)</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input id="email" name="email" type="email" class="validate">
                                                <label for="email">Email</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input id="contact" name="contact" type="text" class="validate">
                                                <label for="contact">Contact</label>
                                            </div>
                                        </div>
                                        <div>
                                            <button type="submit" class="btn btn-primary">Ajouter</button>
                                            <button type="reset" class="btn btn-danger">Annuler</button>
                                        </div>
                                    </form>
                                    <div class="clearBoth"></div>
                                </div>
                            </div>
                        </div>
                    </div>

    <script defer>
        $(document).ready(function () {
            $('#dataTables-example').dataTable();
        });
    </script>
<?php include_once APPPATH . 'Views/_partials/footer.php'; ?>

