<?php include_once APPPATH . 'Views/_partials/header.php' ?>
<?php include_once APPPATH . 'Views/_partials/navbar.php' ?>
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header">
            Liste des employés <a href="/employe/create" class="btn btn-primary">+ Ajouter</a>
        </h1>
    </div>
    <div id="page-inner">

        <div class="row">

            <div class="col-md-12">
                <!--Page content-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- Advanced Tables -->
                        <div class="card">
                            <div class="card-action">
                            </div>
                            <div class="card-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover"
                                           id="dataTables-example">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nom</th>
                                            <th>Prénom(s)</th>
                                            <th>Contact</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $i = 1;
                                        if (isset($employes))
                                            foreach ($employes as $employe):
                                                ?>
                                                <tr>
                                                    <td> <?= $i++ ?></td>
                                                    <td><?= $employe->nom ?></td>
                                                    <td><?= $employe->prenoms ?></td>
                                                    <td><?= $employe->contact ?></td>
                                                    <td>
                                                        <a href="/employe/<?= $employe->id ?>"><i
                                                                    class="fa fa-eye text-primary"></i></a>
                                                        <a href="/employe/edit/<?= $employe->id ?>"><i
                                                                    class="fa fa-edit text-warning"></i></a>
                                                        <a href="/employe/delete/<?= $employe->id ?>"><i
                                                                    class="fa fa-trash-o text-danger"></i></a>
                                                        <!-- TODO: Mettre un formulaire à la place et configurer la route de suppression pour ne répondre qu'aux requêtes 'post' -->

                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                        <!--End Advanced Tables -->
                    </div>
                </div>

                <script defer>
                    $(document).ready(function () {
                        $('#dataTables-example').dataTable();
                    });
                </script>
                <?php include_once APPPATH . 'Views/_partials/footer.php'; ?>

