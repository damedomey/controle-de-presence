<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Employe extends Migration
{
    public function up()
    {
        $this->forge->addField("id int not null auto_increment")
            ->addField("nom varchar(255) not null")
            ->addField("prenoms varchar(255) not null")
            ->addField("email varchar(180) not null")
            ->addField("contact int not null")
            ->addField("created_at datetime not null default now()")
            ->addField("updated_at datetime")
            ->addField("deleted_at datetime")
            ->addPrimaryKey("id")
            ->createTable("employes");
    }

    public function down()
    {
        $this->forge->dropTable("employes");
    }
}
