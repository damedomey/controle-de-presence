<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TypeConge extends Migration
{
    public function up()
    {
        $this->forge->addField("id int not null auto_increment")
            ->addField("title varchar(255) not null unique")
            ->addPrimaryKey('id')
            ->createTable('type_conges');
    }

    public function down()
    {
        $this->forge->dropTable('type_conges');
    }
}
